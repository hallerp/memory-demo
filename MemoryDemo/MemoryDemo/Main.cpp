
// Memory Demo
// Ryan Appel

#include <iostream>
#include <conio.h>

using namespace std;

struct Course
{
	int ID = 0;
	string Title;
	string Description;
};

struct Student
{
	string FirstName;
	string LastName;
	float GPA = 4.0;
	Course Course;
};

Student *CreateStudent() // factory function
{
	Student *pS = new Student;
	cout << "First Name: ";
	cin >> pS->FirstName;

	cout << "Last Name: ";
	cin >> pS->LastName;

	cout << "GPA: ";
	cin >> pS->GPA;

	return pS;
}

void PrintStudent(Student *pS)
{
	cout << pS->FirstName << " " << pS->LastName << ": " << pS->GPA << "\n";
}

int main()
{

	char input = 'n';
	cout << "Create a student? (y): ";
	cin >> input;

	Student *pS1 = nullptr;
	

	if (input == 'y')
	{
		pS1 = CreateStudent();
	}

	

	if (pS1) // null check
	{
		PrintStudent(pS1);
		//pS1->FirstName = "Jimmy";
		//pS1->LastName = "G";

		delete pS1;
	}



	(void)_getch();
	return 0;
}