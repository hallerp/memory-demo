
// Arrays and Vectors
// Paul Haller

#include <iostream>
#include <conio.h>
#include <iomanip>

using namespace std;

struct Person
{
	string FirstName;
};

void PrintNumbers(int *pNumbers, int size)
{
	for (int i = 0; i < size; i++)
	{
		cout << pNumbers[i];

		if (i == size - 1) cout << "\n";
		else cout << ", ";
	}
}

void ArraysOnStack()
{

	const int SIZE = 3;
	int numbers[SIZE]; // a pointer to an array

	cout << numbers << "\n"; // these are the same
	cout << &numbers[0] << "\n";
	
	//numbers[0] = 5;
	//numbers[1] = 5;
	//numbers[2] = 5;
	//numbers[3] = 5;
	//same as below

	for (int i = 0; i < SIZE; i++)
	{
		cout << (i + 1) << ": ";
		cin >> numbers[i];
	}

	for (int i = 0; i < SIZE; i++)
	{
		cout << numbers[i];
		cout << (i == SIZE - 1 ? "\n" : ", "); // if (?) else (:)
	}

	//numbers[13] = 2; overrides 13 to 2

	//const int NUM_PEOPLE = 5;
	//Person people[NUM_PEOPLE];

	//for (int i = 0; i < NUM_PEOPLE; i++)
	//{
	//	cout << "Name: ";
	//	cin >> people[i].FirstName;

	//}

	PrintNumbers(numbers, SIZE);
}

void ArraysOnHeap()
{
	int size = 0;
	cout << "How many? ";
	cin >> size;

	int *pNumbers = new int[size];

	//for (int i = 0; i < size; i++)
	//{
	//	cout << (i + 1) << ": ";
	//	cin >> pNumbers[i];
	//}
	PrintNumbers(pNumbers, size);
}

void CharArrays()
{
	char longname[] = { 'R', 'y', 'a', 'n', '\0' }; // same as below
	char name[5] = "Ryan"; // null terminated string adds \0 to the end of the array
	cout << name;
}


int main()
{
	// ArraysOnStack();

	// ArraysOnHeap();

	CharArrays();

	cout << "2345 Ricardo Ortiz $1234.90\n";
	cout << "2345 Ricardo Ortiz $1234.90\n";
	cout << "2345 Ricardo Ortiz $1234.90\n";
	cout << "Total gross pay: $ 5634.90\n";

	float n = 12.5;

	cout << "$";
	cout << setprecision(2) << fixed << n;

	(void)_getch();
	return 0;
}